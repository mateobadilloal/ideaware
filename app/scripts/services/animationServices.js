angular.module('ideaware')
    .constant("baseURL", "http://localhost:3000/")
    .factory('footerAnimFactory', [function () {
        var footerfac = {};

        footerfac.scaffoldAnims = function () {
            const twitterBubble = document.querySelector('#twitterBubble');
            const twitterIcon = document.querySelector('#twitterIcon');
            const twitterContainer = document.querySelector('#twitter');

            const facebookBubble = document.querySelector('#facebookBubble');
            const facebookIcon = document.querySelector('#facebookIcon');
            const facebookContainer = document.querySelector('#facebook');

            const youtubeBubble = document.querySelector('#youtubeBubble');
            const youtubeIcon = document.querySelector('#youtubeIcon');
            const youtubeContainer = document.querySelector('#youtube');


            const COLORS = {
                white: "#fff",
                transparent: 'none'
            }

            const CIRCLE_OPTS = {
                shape: 'circle',
                fill: 'none',
                radius: 11,
            }

            const SOCIAL_ICON_OPTS = {
                radius: 20,
                scale: {
                    0.5: 0.4
                },
                fillOpacity: {
                    1: 0.9
                },
                isShowStart: true,
                duration: 300
            }

            const SOCIAL_ICON_OVER_OPTS = {
                radius: 20,
                opacity: {
                    0: 1
                },
                scale: 0.4,
                fillOpacity: 0.9,
                duration: 100
            }

            class twitter extends mojs.CustomShape {
                getShape() {
                    return '<path d="M110.8 10.7c-4.1 1.8-8.5 3-13.1 3.6 4.7-2.8 8.3-7.3 10-12.6-4.4 2.6-9.3 4.5-14.4 5.5C89.1 2.8 83.2 0 76.7 0 64.1 0 54 10.2 54 22.7c0 1.8.2 3.5.6 5.2-18.9-.9-35.7-10-46.9-23.7-2 3.4-3.1 7.3-3.1 11.4 0 7.9 4 14.8 10.1 18.9-3.7-.1-7.2-1.1-10.3-2.8v.3c0 11 7.8 20.2 18.2 22.3-1.9.5-3.9.8-6 .8-1.5 0-2.9-.1-4.3-.4 2.9 9 11.3 15.6 21.2 15.8-7.8 6.1-17.6 9.7-28.2 9.7-1.8 0-3.6-.1-5.4-.3C10.1 86.3 22 90 34.8 90c41.8 0 64.7-34.6 64.7-64.7 0-1 0-2-.1-2.9 4.5-3.2 8.3-7.2 11.4-11.7"/>'
                }
                getLength() {
                    return 200;
                }
            }
            class facebook extends mojs.CustomShape {
                getShape() {
                    return '<path d="M63.9,44.8h-23V33.6c0,0-1.3-10.7,6.2-10.7c8.4,0,15.1,0,15.1,0V0H36.4c0,0-21.6-0.1-21.6,21.6c0,4.6,0,13.1-0.1,23.3H0 v18.5h14.8c-0.1,29.4-0.2,62.2-0.2,62.2h26.3V63.3h17.4L63.9,44.8z"/>'
                }
                getLength() {
                    return 200;
                }
            }

            class youtube extends mojs.CustomShape {
                getShape() {
                    return '<path d="M77.933,0.773C67.851,0.024,48.981-0.181,33.721,0.16C27.386,0.296,21.663,0.501,17.44,0.773 C1.975,1.795,0.136,9.356,0,34.085c0.136,24.661,1.908,32.29,17.44,33.312c4.224,0.272,9.946,0.477,16.282,0.613 c15.26,0.341,34.13,0.136,44.212-0.613c15.464-1.022,17.303-8.584,17.44-33.312C95.236,9.425,93.465,1.795,77.933,0.773z M34.062,50.026V18.144l33.517,15.941L34.062,50.026z"/>'
                }
                getLength() {
                    return 200;
                }
            }

            mojs.addShape('twitter', twitter);
            mojs.addShape('facebook', facebook);
            mojs.addShape('youtube', youtube);

            const socialCircle = function (par, color) {
                const shape = new mojs.Shape({
                    ...CIRCLE_OPTS,
                    shape: 'circle',
                        parent: par,
                        stroke: color,
                        strokeWidth: {
                            22: 1
                        },
                        scale: {
                            1: 2,
                            esing: 'sin.out'
                        },
                        duration: 300,
                        isShowStart: true
                })

                return shape;

            }

            const socialCircleOv = function (par, color) {
                const shape = new mojs.Shape({
                    ...CIRCLE_OPTS,
                    parent: par,
                        stroke: color,
                        strokeWidth: 1,
                        opacity: {
                            0: 1
                        },
                        scale: 2,
                        duration: 100,
                })

                return shape;

            }

            const socialIcon = function (par, shapeName, colorEnd) {
                const shape = new mojs.Shape({
                    ...SOCIAL_ICON_OPTS,
                    parent: par,
                        shape: shapeName,
                        fill: {
                            '#508397': colorEnd
                        },

                })
                return shape;
            }

            const socialIconOver = function (par, shapeName, colorEnd) {
                const shape = new mojs.Shape({
                    ...SOCIAL_ICON_OVER_OPTS,
                    parent: par,
                        shape: shapeName,
                        fill: colorEnd,
                })

                return shape;

            }

            const twitterCircle = socialCircle(twitterBubble, COLORS.white);
            const twitterCircleOver = socialCircleOv(twitterBubble, COLORS.white);
            const twitterShape = socialIcon(twitterIcon, 'twitter', '#fff');
            const twitterShapeOver = socialIconOver(twitterIcon, 'twitter', COLORS.white);

            const youtubeCircle = socialCircle(youtubeBubble, COLORS.white);
            const youtubeCircleOver = socialCircleOv(youtubeBubble, COLORS.white);

            const facebookCircle = socialCircle(facebookBubble, COLORS.white);
            const facebookCircleOver = socialCircleOv(facebookBubble, COLORS.white);

            const facebookShape = new mojs.Shape({

                parent: facebookIcon,
                shape: 'facebook',
                fill: {
                    '#508397': '#fff'
                },
                x: {
                    4: 3
                },
                y: '-1',
                ...SOCIAL_ICON_OPTS
            });
            const facebookShapeOver = new mojs.Shape({
                parent: facebookIcon,
                shape: 'facebook',
                fill: COLORS.white,
                x: {
                    4: 3
                },
                y: '-1',
                ...SOCIAL_ICON_OVER_OPTS
            })
            
            const youtubeShape = new mojs.Shape({

                parent: youtubeIcon,
                shape: 'youtube',
                fill: {
                    '#508397': '#fff'
                },
                y: '2.5',
                ...SOCIAL_ICON_OPTS
            });
            const youtubeShapeOver = new mojs.Shape({
                parent: youtubeIcon,
                shape: 'youtube',
                fill: COLORS.white,
                y: '2.5',
                
                ...SOCIAL_ICON_OVER_OPTS
            })


            facebookIcon.addEventListener('mouseenter', e => {
                facebookCircle
                    .play();
                facebookShape
                    .play();
            })
            facebookIcon.addEventListener('mouseleave', e => {
                facebookCircle
                    .playBackward();
                facebookShape
                    .playBackward();
            })
            facebookIcon.addEventListener('click', e => {
                facebookCircleOver
                    .then({
                        opacity: {
                            to: 0
                        }
                    })
                    .replay();
                facebookShapeOver
                    .then({
                        opacity: {
                            to: 0
                        }
                    })
                    .replay();
            })

            twitterIcon.addEventListener('mouseenter', e => {
                twitterCircle
                    .play();
                twitterShape
                    .play();
            })
            twitterIcon.addEventListener('mouseleave', e => {
                twitterCircle
                    .playBackward();
                twitterShape
                    .playBackward();
            })
            twitterIcon.addEventListener('click', e => {
                twitterCircleOver
                    .then({
                        opacity: {
                            to: 0
                        }
                    })
                    .replay();
                twitterShapeOver
                    .then({
                        opacity: {
                            to: 0
                        }
                    })
                    .replay();
            })

            youtubeIcon.addEventListener('mouseenter', e => {
                youtubeCircle
                    .play();
                youtubeShape
                    .play();
            })
            youtubeIcon.addEventListener('mouseleave', e => {
                youtubeCircle
                    .playBackward();
                youtubeShape
                    .playBackward();
            })
            youtubeIcon.addEventListener('click', e => {
                youtubeCircleOver
                    .then({
                        opacity: {
                            to: 0
                        }
                    })
                    .replay();
                youtubeShapeOver
                    .then({
                        opacity: {
                            to: 0
                        }
                    })
                    .replay();
            })
        }



        return footerfac;
    }])

;