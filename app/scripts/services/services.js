'use strict';

angular.module('ideaware')
    .constant("baseURL", "http://localhost:3000/")
    .service('serviceTemp', ['$resource' ,'baseURL', function ($resource,baseURL) {

        this.metodoUno = function () {
            return $resource(baseURL + "path/to/service", null, {
                'update': {
                    method: 'METODO' 
                }
            });
        };


    }])

.factory('factoryTemp', ['$resource', 'baseURL', function ($resource, baseURL) {

    var temFact = {};


    temFact.metodoUno = function () {
        return $resource(baseURL + "path/to/service", null, {
                'update': {
                    method: 'METODO'
                }
            });
    };
    return temFact;
}])

;