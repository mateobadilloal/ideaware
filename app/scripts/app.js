'use strict';

angular.module('ideaware', ['ui.router', 'ngResource', 'jtt_youtube', 'oblador.lazytube', 'infinite-scroll'])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        // route for the home page
            .state('landing', {
            url: '/landing',
            views: {
                'header': {
                    templateUrl: 'views/header.html',
                    controller: 'HeaderController'
                },
                'content@': {
                    templateUrl: 'views/landing.html',
                    controller: 'LandingController'
                },
                'footer': {
                    templateUrl: 'views/footer.html',
                    controller: 'FooterController'
                },
                
            }

        });

        $urlRouterProvider.otherwise('/landing');
    });