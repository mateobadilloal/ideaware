'use strict';

angular.module('ideaware')

.controller('HeaderController', ['$scope', function ($scope) {

        $scope = 3;
    }])
    .controller('LandingController', ['$scope', 'youtubeFactory', function ($scope, youtubeFactory) {

        $scope.query = "cats";
        $scope.lazyVideos = [];
        $scope.totalVideos = [];
        $scope.category = 1;
        var count = 0;

        $scope.categories = [
            {
                "id": "1",
                "title": "Film & Animation"
            },
            {
                "id": "2",
                "title": "Autos & Vehicles"
            },
            {
                "id": "10",
                "title": "Music"
            },
            {
                "id": "15",
                "title": "Pets & Animals"
            },
            {
                "id": "17",
                "title": "Sports"
            },
            {
                "id": "18",
                "title": "Short Movies"
            },
            {
                "id": "19",
                "title": "Travel & Events"
            },
            {
                "id": "20",
                "title": "Gaming"
            },
            {
                "id": "21",
                "title": "Videoblogging"
            },
            {
                "id": "22",
                "title": "People & Blogs"
            },
            {
                "id": "23",
                "title": "Comedy"
            },
            {
                "id": "24",
                "title": "Entertainment"
            },
            {
                "id": "25",
                "title": "News & Politics"
            },
            {
                "id": "26",
                "title": "Howto & Style"
            },
            {
                "id": "27",
                "title": "Education"
            },
            {
                "id": "28",
                "title": "Science & Technology"
            },
            {
                "id": "29",
                "title": "Nonprofits & Activism"
            },
            {
                "id": "30",
                "title": "Movies"
            },
            {
                "id": "31",
                "title": "Anime/Animation"
            },
            {
                "id": "32",
                "title": "Action/Adventure"
            },
            {
                "id": "33",
                "title": "Classics"
            },
            {
                "id": "34",
                "title": "Comedy"
            },
            {
                "id": "35",
                "title": "Documentary"
            },
            {
                "id": "36",
                "title": "Drama"
            },
            {
                "id": "37",
                "title": "Family"
            },
            {
                "id": "38",
                "title": "Foreign"
            },
            {
                "id": "39",
                "title": "Horror"
            },
            {
                "id": "40",
                "title": "Sci-Fi/Fantasy"
            },
            {
                "id": "41",
                "title": "Thriller"
            },
            {
                "id": "42",
                "title": "Shorts"
            },
            {
                "id": "43",
                "title": "Shows"
            },
            {
                "id": "44",
                "title": "Trailers"
            }
        ];

        $scope.changeCategory = function () {
            $scope.category = $scope.video.category;
            $scope.searchVideos();
        };

        $scope.searchVideos = function () {
            count = 0;
            $scope.totalVideos = [];
            $scope.lazyVideos = [];
            youtubeFactory.getVideosFromSearchByParams({
                q: $scope.query,
                key: 'AIzaSyCyHNIddSq6ukp4keGiz8G7c2CpAGzfHHQ',
                maxResults: 50,
                videoCategoryId: $scope.category
            }).then(function (result) {
                    for (var i = 0; i < result.data.items.length; i++) {
                        $scope.totalVideos.push(result.data.items[i].id.videoId);
                    }

                    for (var j = 0; j < 10; j++) {
                        $scope.lazyVideos.push(result.data.items[j].id.videoId);
                    }

                },
                function (result) {
                    console.log(result);
                });

        };

        $scope.loadTen = function () {
            count++;
            if (count < 4) {
                for (var i = count * 10; i < (count + 1) * 10; i++) {
                    $scope.lazyVideos.push($scope.totalVideos[i]);
                }
            }
        };


            }])
    .controller('FooterController', ['$scope', 'footerAnimFactory', function ($scope, footerAnimFactory) {
        footerAnimFactory.scaffoldAnims();
    }])



;