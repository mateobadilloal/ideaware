"use strict";

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

angular.module('ideaware').constant("baseURL", "http://localhost:3000/").factory('footerAnimFactory', [function () {
    var footerfac = {};

    footerfac.scaffoldAnims = function () {
        var twitterBubble = document.querySelector('#twitterBubble');
        var twitterIcon = document.querySelector('#twitterIcon');
        var twitterContainer = document.querySelector('#twitter');

        var facebookBubble = document.querySelector('#facebookBubble');
        var facebookIcon = document.querySelector('#facebookIcon');
        var facebookContainer = document.querySelector('#facebook');

        var youtubeBubble = document.querySelector('#youtubeBubble');
        var youtubeIcon = document.querySelector('#youtubeIcon');
        var youtubeContainer = document.querySelector('#youtube');

        var COLORS = {
            white: "#fff",
            transparent: 'none'
        };

        var CIRCLE_OPTS = {
            shape: 'circle',
            fill: 'none',
            radius: 11
        };

        var SOCIAL_ICON_OPTS = {
            radius: 20,
            scale: {
                0.5: 0.4
            },
            fillOpacity: {
                1: 0.9
            },
            isShowStart: true,
            duration: 300
        };

        var SOCIAL_ICON_OVER_OPTS = {
            radius: 20,
            opacity: {
                0: 1
            },
            scale: 0.4,
            fillOpacity: 0.9,
            duration: 100
        };

        var twitter = function (_mojs$CustomShape) {
            _inherits(twitter, _mojs$CustomShape);

            function twitter() {
                _classCallCheck(this, twitter);

                return _possibleConstructorReturn(this, (twitter.__proto__ || Object.getPrototypeOf(twitter)).apply(this, arguments));
            }

            _createClass(twitter, [{
                key: "getShape",
                value: function getShape() {
                    return '<path d="M110.8 10.7c-4.1 1.8-8.5 3-13.1 3.6 4.7-2.8 8.3-7.3 10-12.6-4.4 2.6-9.3 4.5-14.4 5.5C89.1 2.8 83.2 0 76.7 0 64.1 0 54 10.2 54 22.7c0 1.8.2 3.5.6 5.2-18.9-.9-35.7-10-46.9-23.7-2 3.4-3.1 7.3-3.1 11.4 0 7.9 4 14.8 10.1 18.9-3.7-.1-7.2-1.1-10.3-2.8v.3c0 11 7.8 20.2 18.2 22.3-1.9.5-3.9.8-6 .8-1.5 0-2.9-.1-4.3-.4 2.9 9 11.3 15.6 21.2 15.8-7.8 6.1-17.6 9.7-28.2 9.7-1.8 0-3.6-.1-5.4-.3C10.1 86.3 22 90 34.8 90c41.8 0 64.7-34.6 64.7-64.7 0-1 0-2-.1-2.9 4.5-3.2 8.3-7.2 11.4-11.7"/>';
                }
            }, {
                key: "getLength",
                value: function getLength() {
                    return 200;
                }
            }]);

            return twitter;
        }(mojs.CustomShape);

        var facebook = function (_mojs$CustomShape2) {
            _inherits(facebook, _mojs$CustomShape2);

            function facebook() {
                _classCallCheck(this, facebook);

                return _possibleConstructorReturn(this, (facebook.__proto__ || Object.getPrototypeOf(facebook)).apply(this, arguments));
            }

            _createClass(facebook, [{
                key: "getShape",
                value: function getShape() {
                    return '<path d="M63.9,44.8h-23V33.6c0,0-1.3-10.7,6.2-10.7c8.4,0,15.1,0,15.1,0V0H36.4c0,0-21.6-0.1-21.6,21.6c0,4.6,0,13.1-0.1,23.3H0 v18.5h14.8c-0.1,29.4-0.2,62.2-0.2,62.2h26.3V63.3h17.4L63.9,44.8z"/>';
                }
            }, {
                key: "getLength",
                value: function getLength() {
                    return 200;
                }
            }]);

            return facebook;
        }(mojs.CustomShape);

        var youtube = function (_mojs$CustomShape3) {
            _inherits(youtube, _mojs$CustomShape3);

            function youtube() {
                _classCallCheck(this, youtube);

                return _possibleConstructorReturn(this, (youtube.__proto__ || Object.getPrototypeOf(youtube)).apply(this, arguments));
            }

            _createClass(youtube, [{
                key: "getShape",
                value: function getShape() {
                    return '<path d="M77.933,0.773C67.851,0.024,48.981-0.181,33.721,0.16C27.386,0.296,21.663,0.501,17.44,0.773 C1.975,1.795,0.136,9.356,0,34.085c0.136,24.661,1.908,32.29,17.44,33.312c4.224,0.272,9.946,0.477,16.282,0.613 c15.26,0.341,34.13,0.136,44.212-0.613c15.464-1.022,17.303-8.584,17.44-33.312C95.236,9.425,93.465,1.795,77.933,0.773z M34.062,50.026V18.144l33.517,15.941L34.062,50.026z"/>';
                }
            }, {
                key: "getLength",
                value: function getLength() {
                    return 200;
                }
            }]);

            return youtube;
        }(mojs.CustomShape);

        mojs.addShape('twitter', twitter);
        mojs.addShape('facebook', facebook);
        mojs.addShape('youtube', youtube);

        var socialCircle = function socialCircle(par, color) {
            var shape = new mojs.Shape(_extends({}, CIRCLE_OPTS, {
                shape: 'circle',
                parent: par,
                stroke: color,
                strokeWidth: {
                    22: 1
                },
                scale: {
                    1: 2,
                    esing: 'sin.out'
                },
                duration: 300,
                isShowStart: true
            }));

            return shape;
        };

        var socialCircleOv = function socialCircleOv(par, color) {
            var shape = new mojs.Shape(_extends({}, CIRCLE_OPTS, {
                parent: par,
                stroke: color,
                strokeWidth: 1,
                opacity: {
                    0: 1
                },
                scale: 2,
                duration: 100
            }));

            return shape;
        };

        var socialIcon = function socialIcon(par, shapeName, colorEnd) {
            var shape = new mojs.Shape(_extends({}, SOCIAL_ICON_OPTS, {
                parent: par,
                shape: shapeName,
                fill: {
                    '#508397': colorEnd
                }

            }));
            return shape;
        };

        var socialIconOver = function socialIconOver(par, shapeName, colorEnd) {
            var shape = new mojs.Shape(_extends({}, SOCIAL_ICON_OVER_OPTS, {
                parent: par,
                shape: shapeName,
                fill: colorEnd
            }));

            return shape;
        };

        var twitterCircle = socialCircle(twitterBubble, COLORS.white);
        var twitterCircleOver = socialCircleOv(twitterBubble, COLORS.white);
        var twitterShape = socialIcon(twitterIcon, 'twitter', '#fff');
        var twitterShapeOver = socialIconOver(twitterIcon, 'twitter', COLORS.white);

        var youtubeCircle = socialCircle(youtubeBubble, COLORS.white);
        var youtubeCircleOver = socialCircleOv(youtubeBubble, COLORS.white);

        var facebookCircle = socialCircle(facebookBubble, COLORS.white);
        var facebookCircleOver = socialCircleOv(facebookBubble, COLORS.white);

        var facebookShape = new mojs.Shape(_extends({

            parent: facebookIcon,
            shape: 'facebook',
            fill: {
                '#508397': '#fff'
            },
            x: {
                4: 3
            },
            y: '-1'
        }, SOCIAL_ICON_OPTS));
        var facebookShapeOver = new mojs.Shape(_extends({
            parent: facebookIcon,
            shape: 'facebook',
            fill: COLORS.white,
            x: {
                4: 3
            },
            y: '-1'
        }, SOCIAL_ICON_OVER_OPTS));

        var youtubeShape = new mojs.Shape(_extends({

            parent: youtubeIcon,
            shape: 'youtube',
            fill: {
                '#508397': '#fff'
            },
            y: '2.5'
        }, SOCIAL_ICON_OPTS));
        var youtubeShapeOver = new mojs.Shape(_extends({
            parent: youtubeIcon,
            shape: 'youtube',
            fill: COLORS.white,
            y: '2.5'

        }, SOCIAL_ICON_OVER_OPTS));

        facebookIcon.addEventListener('mouseenter', function (e) {
            facebookCircle.play();
            facebookShape.play();
        });
        facebookIcon.addEventListener('mouseleave', function (e) {
            facebookCircle.playBackward();
            facebookShape.playBackward();
        });
        facebookIcon.addEventListener('click', function (e) {
            facebookCircleOver.then({
                opacity: {
                    to: 0
                }
            }).replay();
            facebookShapeOver.then({
                opacity: {
                    to: 0
                }
            }).replay();
        });

        twitterIcon.addEventListener('mouseenter', function (e) {
            twitterCircle.play();
            twitterShape.play();
        });
        twitterIcon.addEventListener('mouseleave', function (e) {
            twitterCircle.playBackward();
            twitterShape.playBackward();
        });
        twitterIcon.addEventListener('click', function (e) {
            twitterCircleOver.then({
                opacity: {
                    to: 0
                }
            }).replay();
            twitterShapeOver.then({
                opacity: {
                    to: 0
                }
            }).replay();
        });

        youtubeIcon.addEventListener('mouseenter', function (e) {
            youtubeCircle.play();
            youtubeShape.play();
        });
        youtubeIcon.addEventListener('mouseleave', function (e) {
            youtubeCircle.playBackward();
            youtubeShape.playBackward();
        });
        youtubeIcon.addEventListener('click', function (e) {
            youtubeCircleOver.then({
                opacity: {
                    to: 0
                }
            }).replay();
            youtubeShapeOver.then({
                opacity: {
                    to: 0
                }
            }).replay();
        });
    };

    return footerfac;
}]);