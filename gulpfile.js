"use strict"

var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    changed = require('gulp-changed'),
    rev = require('gulp-rev'),
    browserSync = require('browser-sync'),
    del = require('del'),
    ngannotate = require('gulp-ng-annotate'),
    babel = require("gulp-babel"),
    babelMin = require("gulp-babel-minify"),
 sourcemaps = require("gulp-sourcemaps");

gulp.task('jshint', function() {
    return gulp.src('app/scripts/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

// Babel
gulp.task("babel", function() {
    return gulp.src("./app/scripts/services/animationServices.js")
        .pipe(babel({
        presets: ['es2015', 'stage-2']
    }))
    .pipe(gulp.dest('.tmp/animation'));
       
});

gulp.task('min', ['babel'], function() {
    return gulp.src('.tmp/animation/*.js')
        .pipe(babelMin())
        .pipe(gulp.dest('dist/scripts/services/'));
})

gulp.task('minDev', ['babel'], function() {
    return gulp.src('.tmp/animation/*.js')
        .pipe(babelMin())
        .pipe(gulp.dest('app/scripts/dev/'));
})

// Clean
gulp.task('clean', function() {
    return del(['dist', '.tmp']);
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('min', 'usemin', 'imagemin', 'copyfonts', 'copyanimations');
});
gulp.task('usemin', ['jshint'], function() {
    return gulp.src('!./app/**/*.html')
        .pipe(usemin({
            css: [minifycss(), rev()],
            js: [ngannotate(), uglify(), rev()]
    }, {debugging: true}))
        .pipe(gulp.dest('dist/'));
});

// Images
gulp.task('imagemin', function() {
    return del(['dist/images']), gulp.src('app/images/**/*')
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(gulp.dest('dist/images'))
        .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('copyfonts', ['clean'], function() {
    gulp.src('./app/fonts/**/*.{ttf,woff,eof,svg}*')
        .pipe(gulp.dest('./dist/fonts'));
});
//Animatios
gulp.task('copyanimations', ['clean'], function() {
    gulp.src('./app/scripts/custom/anim*.js')
        .pipe(gulp.dest('./dist/scripts/custom'));
    gulp.src('./bower_components/bootstrap/dist/fonts/**/*.{ttf,woff,eof,svg}*')
        .pipe(gulp.dest('./dist/fonts'));
});
// Watch Babel
gulp.task('watchBabel',  function() {
    // Watch babel files
    gulp.watch('app/scripts/services/animationServices.js', ['minDev']);


});
// Watch
gulp.task('watch', ['browser-sync'], function() {
    // Watch babel files
    gulp.watch('app/scripts/services/animationServices.js', ['min']);
    // Watch .js files
    gulp.watch('{app/scripts/**/*.js,app/styles/**/*.css,app/**/*.html,!app/scripts/services/animationServices.js}', ['usemin']);
    // Watch image files
    gulp.watch('app/images/**/*', ['imagemin']);

});

gulp.task('browser-sync', ['default'], function() {

    var files = [
        'app/**/*.html',
        'app/styles/**/*.css',
        'app/images/**/*.png',
        'app/scripts/**/*.js',
        'dist/**/*'
    ];

    browserSync.init(files, {
        server: {
            baseDir: "dist",
            index: "index.html"
        }
    });
    // Watch any files in dist/, reload on change
    gulp.watch(['dist/**']).on('change', browserSync.reload);
});